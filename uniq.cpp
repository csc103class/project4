#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using std::cin;
using std::cout;

using namespace std;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of uniq.  Supported options:\n\n"
"   -c,--count         prefix lines by their counts.\n"
"   -d,--repeated      only print duplicate lines.\n"
"   -u,--unique        only print lines that are unique.\n"
"   --help             show this message and exit.\n";

/* ----------------  PROTOTYPE Functions ------------------------ */
size_t count_uniq( string& );
//size_t print_duplicate( string&);
//size_t print_uniq(string&);
/* ------------------------- END --------------------------------- */

/* -------------------  MAIN Function ---------------------------- */
int main(int argc, char *argv[]) {
	// define long options
	static int showcount=0, dupsonly=0, uniqonly=0;

	static struct option long_opts[] = {
		{"count",         no_argument, 0, 'c'},
		{"repeated",      no_argument, 0, 'd'},
		{"unique",        no_argument, 0, 'u'},
		{"help",          no_argument, 0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cduh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				showcount = 1;
				break;
			case 'd':
				dupsonly = 1;
				break;
			case 'u':
				uniqonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
#if 0
	/* TODO: write me... */
    FILE *f = &stdin;
	while ( f(c,d,u,h) > 0){
		if (showcount == 1) uniq_count(f);
		
	}
#endif

	string words;
	cout <<" enter text " << endl;
	cin>>words;

	uniq_count(words);

	while (cin>>words && words != "qu") {
	//uniq_count(words);
	}

	
return 0;
}
/* ------------------------------ END MAIN --------------------------------- */

/* ---------------------------- HEADER Functions ----------------------- */
 
size_t uniq_count(string& str){
size_t count = 0;
char ch;
size_t str_size = str.length();
for (size_t i = 0; i < str_size; i++) {
	if(str[i] == 0) return 0;
	if(str[i] == str[i+1]) count++;
	if(str[i] == '\n' ) cout << count ;
}
 return count;
}
